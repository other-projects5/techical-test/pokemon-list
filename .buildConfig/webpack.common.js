const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (envVars) => {
    const { env } = envVars;

    return {
        entry: path.resolve(__dirname, '..', 'src', 'index.tsx'),
        output: {
            path: path.resolve(__dirname, '..', 'build'),
            filename: 'bundle.js',
            publicPath: '/',
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                        },
                    ],
                },
                {
                    test: /\.(ts|tsx)$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'ts-loader',
                        },
                    ],
                },
                {
                    test: /\.css$/,
                    exclude: /node_modules/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                modules: {
                                    localIdentName: env === 'development' ? '[name]-[local]' : '[hash:base64:5]'
                                },
                                importLoaders: 2,
                                // localsConvention: 'camelCase',
                                sourceMap: env === 'development'
                            }
                        },
                        {
                            loader: 'sass-loader',
                            // options: {
                            //   implementation: require.resolve("sass"),
                            // }
                        },
                        {
                            loader: 'sass-resources-loader',
                            options: {
                                resources: path.resolve(__dirname, '..', 'src', 'styles', '_variables.scss')
                            }
                        }
                    ],
                },
                {
                    test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                    type: 'asset/resource',
                },
                {
                    test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                    type: 'asset/inline',
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                inject: true,
                title: 'Pokemon test',
                template: path.resolve(__dirname, '..', 'src', 'index.html'),
            }),
            new MiniCssExtractPlugin({
                filename: 'style.css'
            })
        ],
        stats: 'errors-only',
    };
};
