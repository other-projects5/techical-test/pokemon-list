
const path = require("path");

module.exports = {
	stories: [
		"../../src/**/*.stories.mdx",
		"../../src/**/*.stories.@(js|jsx|ts|tsx)"
	],
	addons: [
		"@storybook/addon-links",
		"@storybook/addon-essentials"
	],
	core: {
		"builder": "webpack5"
	},

	webpackFinal: async (config) => {
		return {
			...config,
			resolve: {
				extensions: ['.tsx', '.ts', '.js'],
			},
			module: {
				rules: [
					{
						test: /\.js$/,
						exclude: /node_modules/,
						use: [
							{
								loader: 'babel-loader',
							},
						],
					},
					{
						test: /\.(ts|tsx)$/,
						exclude: /node_modules/,
						use: [
							{
								loader: 'ts-loader',
							},
						],
					},
					{
						test: /\.css$/,
						exclude: /node_modules/,
						use: [
							'style-loader',
							{
								loader: 'css-loader',
								options: {
									modules: true
								}
							}
						]
					},
					{
						test: /\.scss$/,
						exclude: /node_modules/,
						use: [
							'style-loader',
							{
								loader: 'css-loader',
								options: {
									modules: {
										localIdentName: '[name]-[local]'
									},
									importLoaders: 2,
									sourceMap: true
								}
							},
							{
								loader: 'sass-loader',
							},
							{
								loader: 'sass-resources-loader',
								options: {
									resources: [
										path.resolve(__dirname, '..', '..', 'src', 'styles', '_variables.scss'),
										path.resolve(__dirname, '..', '..', 'src', 'styles', 'main.scss')
									]
								}
							}
						],
					},
					{
						test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
						type: 'asset/resource',
					},
					{
						test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
						type: 'asset/inline',
					},
				],
			}
		};
	}
};
