const webpack = require('webpack');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

module.exports = {
  mode: 'development',
  devtool: 'cheap-module-source-map',
  devServer: {
    port: 3000,
    hot: true,
    open: true,
    historyApiFallback: true,
  },
  plugins: [
    new ReactRefreshWebpackPlugin(),
    new webpack.DefinePlugin({
      // TODO: esto crea una variable que puede ser usada en la aplicacion. Para acceder a ella -> {process.env.name}
      'process.env.name': JSON.stringify('DEV-variable'),
    }),
  ],
};
