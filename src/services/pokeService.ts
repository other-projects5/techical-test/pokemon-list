import httpClient from '../config/axios';

import { IPokemon, IPokemonPagination } from '../models/entities';

export class PokeService {
	static getPokemonList = async (
		limit: number,
		skip: number
	): Promise<IPokemon[]> => {
		const url = `/pokemon?limit=${limit}&offset=${skip}`;
		try {
			const response = await httpClient.get<IPokemonPagination>(url);

			const res: IPokemon[] = [];

			for (const item of response.data.results) {
				res.push(await this.getPokemonDetailsByName(item.name));
			}

			return res;
		} catch (e) {
			console.log('Error', e);
			return [];
		}
	};

	static getPokemonDetailsByName = async (
		name: string
	): Promise<IPokemon> => {
		const url = `/pokemon/${name}`;
		const response = await httpClient.get(url);

		return response.data;
	};
}
