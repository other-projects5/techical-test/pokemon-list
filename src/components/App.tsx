import * as React from 'react';
import { Fragment, FunctionComponent } from 'react';
import { BrowserRouter } from 'react-router-dom';

import { Header } from './partials';
import { Routes } from './routes';

import '../styles/main.scss';

export const App: FunctionComponent = () => {
	return (
		<Fragment>
			<BrowserRouter>
				<Header />
				<Routes />
			</BrowserRouter>
		</Fragment>
	);
};
