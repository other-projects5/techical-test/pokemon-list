import * as React from 'react';
import { FunctionComponent } from 'react';

import { Switch, Route } from 'react-router-dom';

import { HomePage, DetailsPage, NotFoundPage } from '../pages';
import { routes } from './routes';

export const Routes: FunctionComponent = () => {
	const { home, details } = routes;

	return (
		<Switch>
			<Route exact path={home} component={HomePage} />
			<Route exact path={details} component={DetailsPage} />

			<Route path="*" component={NotFoundPage} />
		</Switch>
	);
};
