import * as React from 'react';
import { FunctionComponent } from 'react';

import { Container } from '../../../__lib__/react-components';

import { PkmGrid } from './PkmGrid';

export const HomePage: FunctionComponent = () => {
	return (
		<Container>
			<PkmGrid />
		</Container>
	);
};
