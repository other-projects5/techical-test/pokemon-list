import * as React from 'react';
import { FunctionComponent } from 'react';

import { IPokemonType } from '../../../../../models/entities';

import styles from './PkmCard.module.scss';

interface PkmCardProps {
	image: string;
	name: string;
	types: IPokemonType[];
}

export const PkmCard: FunctionComponent<PkmCardProps> = (props) => {
	const { image, name, types } = props;

	return (
		<div className={styles.card}>
			<img src={image} alt="pkm-image" />
			<div className={styles.name}>{name}</div>
			<div className={styles.typeWrapper}>
				{types.map((item: IPokemonType, index: number) => (
					<div key={index} className={styles.type}>
						{item.type.name}
					</div>
				))}
			</div>
		</div>
	);
};
