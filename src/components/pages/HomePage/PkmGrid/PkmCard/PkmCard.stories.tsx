import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { PkmCard } from './PkmCard';

export default {
	title: 'Partials/PkmCard',
	component: PkmCard,

} as ComponentMeta<typeof PkmCard>;

const Template: ComponentStory<typeof PkmCard> = (args) => (
	<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
		<PkmCard {...args} />
	</div>
);

export const _PkmCard = Template.bind({});
_PkmCard.args = {
	name: 'Test',
	types: [{ slot: 1, type: { name: 'Fairy', url: '...' } }],
	image: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/35.png'
};
