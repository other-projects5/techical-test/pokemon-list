import * as React from 'react';
import { FunctionComponent, useEffect, useState, useRef, MutableRefObject } from 'react';
import { Link } from 'react-router-dom';

import { Col, Row } from '../../../../__lib__/react-components';

import { PokeService } from '../../../../services';
import { routes } from '../../../routes';
import { IPokemon } from '../../../../models/entities';
import { Section } from '../../../templates';
import { PkmCard } from './PkmCard';

import styles from './PkmGrid.module.scss';

export const PkmGrid: FunctionComponent = () => {
	const LIMIT = 30;

	const [data, setData] = useState<IPokemon[]>([]);

	const [loading, setLoading] = useState<boolean>(false);
	const myLoadingRef: MutableRefObject<boolean> = useRef(loading);
	const setMyLoadingRef = (data: boolean): void => {
		myLoadingRef.current = data;
		setLoading(data);
	};

	const [skip, setSkip] = useState<number>(0);
	const mySkipRef: MutableRefObject<number> = useRef(skip);
	const setMySkipRef = (data: number): void => {
		mySkipRef.current = data;
		setSkip(data);
	};


	useEffect(() => {
		addEventListener('scroll', () => onScroll(setMySkipRef, setMyLoadingRef));
		window.scroll({ top: 0 });
		return () => {
			addEventListener('scroll', () => onScroll());
		};
	}, []);

	useEffect(() => {
		loadData();
	}, [skip]);

	const onScroll = (updateSkip?: (value: number) => void, setMyLoadingRef?: (value: boolean) => void): void => {
		if (!updateSkip && !setMyLoadingRef) {
			return;
		}
		if ((window.innerHeight + window.scrollY >= document.body.offsetHeight - 1) && !myLoadingRef!.current) {
			console.log('scroll');
			setMyLoadingRef!(true);
			updateSkip!(LIMIT + mySkipRef.current);
		}
	};

	const loadData = (): void => {
		PokeService.getPokemonList(LIMIT, skip).then((res) => {
			setData([...data, ...res]);
			setMyLoadingRef(false);
		});
	};

	return (
		<Section title="IN LIVE">
			{data.length === 0 ? (
				<div className={styles.loading}>Loading...</div>
			) : (
				<div>
					<Row>
						{data.map((pokemon: IPokemon) => {
							const {
								id,
								name,
								sprites: { front_default },
								types,
							} = pokemon;

							return (
								<Col
									key={`${id}-${name}`}
									xs={6}
									sm={6}
									md={4}
									lg={4}
									xl={2}
									className={styles.col}
								>
									<Link
										className={styles.link}
										to={{
											pathname: routes.details,
											state: { pokemon },
										}}
									>
										<PkmCard
											image={front_default}
											name={name}
											types={types}
										/>
									</Link>
								</Col>
							);
						})}
					</Row>
					{loading && (
						<div className={styles.loadMore}>
							Loading more Pokemons...
						</div>
					)}
				</div>
			)}
		</Section>
	);
};
