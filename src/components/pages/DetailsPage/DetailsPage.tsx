import * as React from 'react';
import { FunctionComponent, useEffect, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import { Col, Container, Row } from '../../../__lib__/react-components';

import { IPokemon } from '../../../models/entities';
import { routes } from '../../routes';

import styles from './Details.module.scss';

interface LocationState {
	pokemon: IPokemon;
}

export const DetailsPage: FunctionComponent = () => {
	const location = useLocation<LocationState>();
	const history = useHistory();

	const [pokemon, setPokemon] = useState<IPokemon>();

	useEffect(() => {
		if (!location.state) {
			history.push(routes.home);
		} else {
			setPokemon(location.state.pokemon);
		}

		window.addEventListener('beforeunload', () => replaceHistory);

		return () => {
			// Reset Location state if we leave this page
			replaceHistory();
			window.removeEventListener('beforeunload', replaceHistory);
		};
	}, []);

	const replaceHistory = (e?: BeforeUnloadEvent): void => {
		if (e) {
			e.preventDefault();
			delete e.returnValue;
		}

		window.history.replaceState({}, document.title);
	};

	return pokemon ? (
		<Container className={styles.container}>
			<Row>
				<Col
					className={styles.col}
					xs={12}
					sm={12}
					md={12}
					lg={6}
					xl={6}
				>
					<img
						className={styles.image}
						src={pokemon.sprites.front_default}
						alt={`${pokemon.name} image`}
					/>
				</Col>
				<Col
					className={styles.col}
					xs={12}
					sm={12}
					md={12}
					lg={6}
					xl={6}
				>
					<div className={styles.name}>{pokemon.name}</div>
					<div className={styles.types}>
						{pokemon.types.map((item, index: number) => (
							<div key={index}>{item.type.name}</div>
						))}
					</div>

					<div className={styles.metadata}>
						<div className={styles.title}>More stats</div>
						<div className={styles.stat}>
							Base experience:{' '}
							<span>{pokemon.base_experience}</span>
						</div>
						<div className={styles.stat}>
							Order: <span>{pokemon.order}</span>
						</div>
						<div className={styles.stat}>
							Height: <span>{pokemon.height}</span>
						</div>
						<div className={styles.stat}>
							Weight: <span>{pokemon.weight}</span>
						</div>
						<div className={styles.stat}>
							Abilities:{' '}
							<span>
								{pokemon.abilities
									.map((ab) => ab.ability.name)
									.join(', ')}
							</span>
						</div>
					</div>
				</Col>
			</Row>
		</Container>
	) : null;
};
