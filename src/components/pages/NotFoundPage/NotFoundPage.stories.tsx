import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { NotFoundPage } from './NotFoundPage';

export default {
	title: 'Pages/NotFoundPage',
	component: NotFoundPage,

} as ComponentMeta<typeof NotFoundPage>;

const Template: ComponentStory<typeof NotFoundPage> = (args) => (
	<NotFoundPage {...args} />
);

export const _NotFoundPage = Template.bind({});
_NotFoundPage.args = {
};
