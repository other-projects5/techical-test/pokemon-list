import * as React from 'react';
import { FunctionComponent } from 'react';

import { Container } from '../../../__lib__/react-components';

import styles from './NotFoundPage.module.scss';

export const NotFoundPage: FunctionComponent = () => {
	return (
		<Container className={styles.containerWrapper}>
			<div className={styles.title}>
				Oops!
			</div>
			<div className={styles.subtitle}>
				404 - PAGE NOT FOUND
			</div>
			<div className={styles.description}>
				<div>
					The page you are looking for might have been removed, had its name changed or is temporarily unavailable.
				</div>
				<div>
					Click on the logo to go to the HOME page.
				</div>
			</div>
		</Container>
	);
};
