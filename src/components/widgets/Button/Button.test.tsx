import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import { Button } from './Button';

configure({ adapter: new Adapter() });

describe('Button', () => {
	test('Should be rendered', () => {
		const wrapper = shallow(<Button text="Test" />);

		expect(wrapper.find({ 'data-test': 'button' }).length).toBe(1);
	});

	test('Action click', () => {
		const mockClick = jest.fn();
		const wrapper = shallow(<Button text="test" onClick={mockClick} />);

		wrapper.find({ 'data-test': 'button' }).simulate('click');

		expect(mockClick.mock.calls.length).toBe(1);
	});
});
