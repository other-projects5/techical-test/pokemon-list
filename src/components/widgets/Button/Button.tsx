import * as React from 'react';
import { FunctionComponent, ButtonHTMLAttributes } from 'react';

import classNames from 'classnames';

import styles from './Button.module.scss';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
	text: string;
	variant?: 'primary' | 'secondary';
	outlined?: boolean;
	rounded?: boolean;
	disabled?: boolean;
}

export const Button: FunctionComponent<ButtonProps> = (props) => {
	const {
		text,
		variant = 'primary',
		outlined,
		rounded,
		disabled,
		...rest
	} = props;

	return (
		<button
			data-test="button"
			className={classNames({
				[styles.button]: true,
				[styles.secondary]: variant === 'secondary',
				[styles.outlined]: outlined,
				[styles.rounded]: rounded,
				[styles.disabled]: disabled,
			})}
			disabled={disabled}
			{...rest}
		>
			{text}
		</button>
	);
};
