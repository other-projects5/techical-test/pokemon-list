import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Button } from './Button';

export default {
	title: 'Widgets/Button',
	component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => (
	<div style={{ display: 'flex', justifyContent: 'center' }}>
		<Button {...args} />
	</div>
);

export const Primary = Template.bind({});
Primary.args = {
	text: 'Click me!',
	onClick: action('Btn clicked'),
};

export const Outlined = Template.bind({});
Outlined.args = {
	text: 'Click me!',
	outlined: true,
	onClick: action('Btn clicked'),
};

export const Secondary = Template.bind({});
Secondary.args = {
	text: 'Click me!',
	variant: 'secondary',
	onClick: action('Btn clicked'),
};

export const Rounded = Template.bind({});
Rounded.args = {
	text: 'Click me!',
	rounded: true,
	onClick: action('Btn clicked'),
};

export const Disabled = Template.bind({});
Disabled.args = {
	text: 'Click me!',
	disabled: true,
	onClick: action('Btn clicked'),
};
