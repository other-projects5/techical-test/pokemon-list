import * as React from 'react';
import { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';

import { Container } from '../../../__lib__/react-components';

import { routes } from '../../routes';

import LogoLetters from '../../../assets/images/logo-letters.png';
import styles from './Header.module.scss';

export const Header: FunctionComponent = () => {
	return (
		<div className={styles.wrapper}>
			<Container className={styles.headerContent}>
				<Link to={routes.home}>
					<img src={LogoLetters} alt="GeekSquare logo" />
				</Link>
			</Container>
		</div>
	);
};
