import * as React from 'react';
import { FunctionComponent } from 'react';

import classNames from 'classnames';
import { Styled } from '../../../__lib__/react-components/utils';

import styles from './Section.module.scss';

interface SectionProps extends Styled {
	id?: string;
	title?: string;
	description?: string;
}
export const Section: FunctionComponent<SectionProps> = (props) => {
	const { style, className, id, title, description, children } = props;

	return (
		<div
			id={id}
			style={style}
			className={classNames({
				[styles.wrapper]: true,
				[className as string]: !!className,
			})}
		>
			{title && <div className={styles.title}>{title}</div>}

			{description && (
				<div className={styles.description}>{description}</div>
			)}

			<div>{children}</div>
		</div>
	);
};
