import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { Section } from './Section';

export default {
	title: 'Templates/Section',
	component: Section,
} as ComponentMeta<typeof Section>;

const Template: ComponentStory<typeof Section> = (args) => (
	<Section {...args} />
);

export const _Section = Template.bind({});
_Section.args = {
	title: "Section's title",
	description: 'This is the description of the section',
	children: (<div>Children content</div>)
};
