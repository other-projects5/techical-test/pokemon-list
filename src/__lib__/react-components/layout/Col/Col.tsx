import * as React from 'react';
import { FunctionComponent } from 'react';

import classNames from 'classnames';

import { Styled } from '../../utils';
import styles from './Col.module.scss';

declare type columnSize = 'xs' | 'sm' | 'md' | 'lg' | 'xl';

interface ColProps extends Styled {
	xs?: number;
	sm?: number;
	md?: number;
	lg?: number;
	xl?: number;
}

export const Col: FunctionComponent<ColProps> = (props) => {
	const { style, className, children } = props;

	const colNames: columnSize[] = ['xs', 'sm', 'md', 'lg', 'xl'];
	const classObj: any = className ? { [className as string]: true } : {};

	colNames.forEach((val) => {
		const colAmount = props[val] || undefined;

		if (colAmount && colAmount > 0 && colAmount <= 12) {
			classObj[styles['col-' + val + '-' + colAmount]] = colAmount;
		}
	});

	return (
		<div style={style} className={classNames(classObj)}>
			<div className={styles.content}>{children}</div>
		</div>
	);
};
