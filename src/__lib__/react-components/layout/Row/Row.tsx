import * as React from 'react';
import { FunctionComponent } from 'react';

import styles from './Row.module.scss';

export const Row: FunctionComponent = (props) => {
	const { children } = props;

	return <div className={styles.row}>{children}</div>;
};
