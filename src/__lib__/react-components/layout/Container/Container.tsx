import * as React from 'react';
import { FunctionComponent } from 'react';

import classNames from 'classnames';

import { Styled } from '../../utils';
import styles from './Container.module.scss';

interface ContainerProps extends Styled {
	fluid?: boolean;
}

export const Container: FunctionComponent<ContainerProps> = (props) => {
	const { children, style, className, fluid } = props;

	return (
		<div
			style={style}
			className={classNames({
				[styles.container]: true,
				[styles.containerFluid]: fluid,
				[className as string]: !!className,
			})}
		>
			{children}
		</div>
	);
};
